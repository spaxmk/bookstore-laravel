@extends('layouts.app')

@section('title','Books')

@section('content')
    <div class="panel panel-default">
        <div class="panel-body">
            <!--
            @foreach ($books as $book)
                    <div class="col-sm-4" style="padding-bottom: 10px">
                        <div class="card">
                            <div>
                                 <img src="{{ Storage::disk('local')->url($book->image) }}"
                                    style="max-width:200px; max-height:200px"/>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">{{$book->marka}} {{$book->oznaka}}</h5>
                                <tr>
                                    <td>Key: {{ $book->id }}</td>
                                    <br>
                                    <td>Title: {{ $book->title }}</td>
                                    <br>
                                    <td>Description: {{ $book->description }}</td>
                                    <br>
                                    <td>Average Rating: {{ $book->getAverageRating() }}</td>
                                    <br>    
                                    <td>Date Created: {{ $book->date_created }}</td>
                                    <br>
                                </tr>
                                <br><br>
                                <a href="{{ url('books/'.$book->id) }}" class="btn btn-default"><i
                                            class="fa fa-btn fa-book"></i>View</a>
                                @endforeach
                            </div>
                        </div>
                -->
            <table class="table table-striped task-table">
                <thead>
                <th>Key</th>
                <th>Title</th>
                <th>Description</th>
                <th>Average Rating</th>
                <th>Date Created</th>
                <th>Actions</th>
                </thead>
                <tbody>
                
                @foreach($books as $book)
                    <tr>
                        <td class="table-text">
                            <div>{{ $book->id }}</div>
                        </td>
                        <td class="table-text">
                            <div><a href="{{ url('books/'.$book->id) }}">{{ $book->title }}</a></div>
                        </td>
                        <td class="table-text">
                            <div>{{ $book->description }}</div>
                        </td>
                        <td class="table-text">
                            <div>{{ $book->getAverageRating() }}</div>
                        </td>
                        <td>
                            <div>{{ date('F d, Y H:i', strtotime($book->date_created)) }}</div>
                        </td>
                        
                            <td>
                                <a href="{{ url('books/'.$book->id) }}" class="btn btn-default"><i
                                            class="fa fa-btn fa-book"></i>View</a>
                            </td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection